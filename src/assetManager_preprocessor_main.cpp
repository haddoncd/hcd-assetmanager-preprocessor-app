#include "common.cpp"
#include "ScopeExit.hpp"
#include "Allocator.hpp"
#include "HeapAllocator.cpp"
#include "PageAllocator.cpp"
#include "Str.cpp"
#include "char.cpp"
#include "path.hpp" // Windows.h & path.cpp are included at end of file
#include "StrBuf.cpp"
#include "print.cpp"
#include "file.cpp"
#include "filesystem.hpp" // Windows.h & filesystem.cpp are included at end of file
#include "List.hpp"
#include "assetManager_config_assetType.cpp"
#include "assetManager_config_parse.cpp"

#include "GL/glcorearb.h"

#define AMD_EXTENSIONS
#define NV_EXTENSIONS
#define ENABLE_HLSL
#include "ShaderLang.h"
#include "localintermediate.h"
#undef AMD_EXTENSIONS
#undef NV_EXTENSIONS
#undef ENABLE_HLSL

#include "glslPreprocessor.cpp"


struct FileAsset
{
  Str name;
  Str path;
};

template <typename A>
ZStr copyToAbsPathZStr(A allocator, Str base_dir, Str path)
{
  ZStr result = ""_z;

  path::Type::Enum path_type = path::getType(path);

  if (path_type != path::Type::INVALID)
  {
    StrBuf::Variable *buf;

    if (path::Type::isAbs[path_type])
    {
      buf = sprint(allocator, path);
    }
    else
    {
      buf = sprint(allocator, base_dir, "\\"_s, path);
    }

    result = buf->zStr();
  }

  return result;
}

int main(int argc, char **argv)
{
  bool success = false;

  PageAllocator al;
  al.init(STD_HEAP);

  BREAKABLE_START
  {
    // initialise glslang
    ShInitialize();
    SCOPE_EXIT(ShFinalize());

    Str pwd = path::workingDir(&al);

    if (argc != 2)
    {
      println(stderr, "Please specify an asset config file"_s);
      break;
    }

    Str config_file_dir = Str::empty();
    {
      Str::MultiFindResult last_slash = zStr(argv[1]).s.findReverse({ '/', '\\' });
      if (last_slash.location != Str::NOT_FOUND)
      {
        config_file_dir = Str{ last_slash.location, argv[1] };
      }

      path::Type::Enum path_type = path::getType(config_file_dir);
      if (path::Type::isRel[path_type])
      {
        config_file_dir = sprint(&al, pwd, "\\"_s, config_file_dir)->str();
      }
    }

    File *config_file_raw = filesystem::load(&al, argv[1]);

    if (!config_file_raw)
    {
      println(stderr, "Failed to load asset config file"_s);
      break;
    }

    bool have_non_shaders = false;
    bool have_raw_files = false;
    List<FileAsset> raw_files;
    bool have_magicavoxel_models = false;
    List<FileAsset> magicavoxel_models;
    bool have_shaders = false;
    glslPreprocessor::ShaderData shaders;

    {
      Str remaining_text = *config_file_raw;

      assetManager::config::Entry entry;

      while (assetManager::config::parse(&remaining_text, &entry, stderr))
      {
        switch (entry.type)
        {
          case assetManager::config::assetType::RAW_FILE:
          {
            auto link = alloc<List<FileAsset>::Link>(&al);
            raw_files.append(link);

            Str path = copyToAbsPathZStr(&al, config_file_dir, entry.params);

            link->data.name = entry.name;
            link->data.path = path;

            have_non_shaders = true;
            have_raw_files = true;
          } break;

          case assetManager::config::assetType::MAGICAVOXEL_MODEL:
          {
            auto link = alloc<List<FileAsset>::Link>(&al);
            magicavoxel_models.append(link);

            Str path = copyToAbsPathZStr(&al, config_file_dir, entry.params);

            link->data.name = entry.name;
            link->data.path = path;

            have_non_shaders = true;
            have_magicavoxel_models = true;
          } break;

          case assetManager::config::assetType::SHADER_PROGRAM:
          {
            glslPreprocessor::ShaderProgram *program =
              shaders.addProgram(&al, entry.name);

            if (!program)
            {
              println(stderr,
                "Skipping duplicate shader program: "_s, entry.name
              );
              continue;
            }

            for (Str remaining_params = entry.params;
                 remaining_params.length;)
            {
              ZStr path = copyToAbsPathZStr(&al,
                                            config_file_dir,
                                            remaining_params.chop(' '));

              if (path == Str::empty()) continue;

              glslPreprocessor::ShaderFile *file =
                shaders.findOrAddFile(&al, path);

              if (!file)
              {
                println(stderr,
                  "Skipping shader with invalid path: "_s, path
                );
                continue;
              }

              if (program->files[file->stage])
              {
                println(stderr,
                  "Skipping duplicate "_s,
                    glslPreprocessor::shaderStage::suffix[file->stage],
                    " stage file: "_s, path
                );
                continue;
              }

              program->files[file->stage] = file;
              have_shaders = true;
            }
          } break;
        }
      }
    }


    ///////////////////////////////
    // Begin printing Assets.cpp //
    ///////////////////////////////

      print(stdout,
R"***(#ifdef assets_cpp
#error Multiple inclusion
#endif
#define assets_cpp

///////////////////////////////////////////////////////////////
// WARNING: This file is generated by the asset preprocessor //
///////////////////////////////////////////////////////////////

)***"_s);


    //////////////////////////////////////
    // Check for errors before printing //
    //////////////////////////////////////

    if (have_shaders)
    {
      shaders.linkPrograms(&al);

      glslPreprocessor::msgType::Enum err_level = glslPreprocessor::msgType::NOTE;

      for (auto *link = shaders.msgs.first();
           link != shaders.msgs.sentinel();
           link = link->next)
      {
        glslPreprocessor::Msg *msg = &link->data;

        if (err_level < msg->type) err_level = msg->type;

        println(stderr,
          glslPreprocessor::msgType::name[msg->type], ": "_s, msg->text
        );
      }

      if (err_level >= glslPreprocessor::msgType::WARN) break;
    }


    //////////////////////////////
    // Assets.cpp prerequisites //
    //////////////////////////////

    print(stdout,
R"***(#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

)***"_s);

    if (have_non_shaders)
    {
      print(stdout,
R"***(#ifndef openglGame_common_hpp
#error "Please include openglGame_common.hpp before this file"
#endif

#ifndef assetManager_config_assetType_hpp
#error "Please include assetManager_config_assetType.hpp before this file"
#endif

#ifndef openglGame_AssetLabel_hpp
#error "Please include openglGame_AssetLabel.hpp before this file"
#endif

)***"_s);
    }

    if (have_raw_files)
    {
      print(stdout,
R"***(#ifndef File_hpp
#error "Please include File.hpp before this file"
#endif

)***"_s);
    }

    if (have_magicavoxel_models)
    {
      print(stdout,
R"***(#ifndef magicaVoxel_Mesh_hpp
#error "Please include magicaVoxel_Mesh.hpp before this file"
#endif

)***"_s);
    }

    if (have_shaders)
    {
      print(stdout,
R"***(#ifndef ScopeExit_hpp
#error "Please include ScopeExit.hpp before this file"
#endif

#ifndef vector_hpp
#error "Please include vector.hpp before this file"
#endif

#ifndef matrix_hpp
#error "Please include matrix.hpp before this file"
#endif

#ifndef gl3w_Functions_hpp
#error "Please include gl3w_Functions.hpp before this file"
#endif

#ifndef shaders_glsl_hpp
#error "Please include shaders_glsl.hpp before this file"
#endif

)***"_s);
    }

    if (have_non_shaders)
    {
      print(stdout,
        "namespace assets\n"
        "{\n"
        "  struct Assets\n"
        "  {\n"_s
      );

      if (have_raw_files)
      {
        for (auto *link = raw_files.first();
             link != raw_files.sentinel();
             link = link->next)
        {
          println(stdout,
            "    File *"_s, link->data.name, ";"_s
          );
        }
      }

      if (have_magicavoxel_models)
      {
        for (auto *link = magicavoxel_models.first();
             link != magicavoxel_models.sentinel();
             link = link->next)
        {
          println(stdout,
            "    magicaVoxel::Mesh *"_s, link->data.name, ";"_s
          );
        }
      }

      print(stdout,
        "  };\n"
        "\n"
        "  struct Statuses\n"
        "  {\n"_s
      );

      if (have_raw_files)
      {
        for (auto *link = raw_files.first();
             link != raw_files.sentinel();
             link = link->next)
        {
          println(stdout,
            "    openglGame::AssetStatus::Enum "_s, link->data.name, ";"_s
          );
        }
      }

      if (have_magicavoxel_models)
      {
        for (auto *link = magicavoxel_models.first();
             link != magicavoxel_models.sentinel();
             link = link->next)
        {
          println(stdout,
            "    openglGame::AssetStatus::Enum "_s, link->data.name, ";"_s
          );
        }
      }

      print(stdout,
        "  };\n"
        "\n"
        "  openglGame::AssetLabel labels[] =\n"
        "  {\n"_s
      );

      {
        bool first = true;

        if (have_raw_files)
        {
          for (auto *link = raw_files.first();
               link != raw_files.sentinel();
               link = link->next)
          {
            if (first)
            {
              first = false;
            }
            else
            {
              println(stdout, ","_s);
            }

            print(stdout,
              "    {\n"
              "      assetManager::config::assetType::RAW_FILE,\n"
              "      R\"***("_s, link->data.name, ")***\"_s,\n"
              "      R\"***("_s, link->data.path, ")***\"_s\n"
              "    }"_s
            );
          }
        }

        if (have_magicavoxel_models)
        {
          for (auto *link = magicavoxel_models.first();
               link != magicavoxel_models.sentinel();
               link = link->next)
          {
            if (first)
            {
              first = false;
            }
            else
            {
              println(stdout, ","_s);
            }

            print(stdout,
              "    {\n"
              "      assetManager::config::assetType::MAGICAVOXEL_MODEL,\n"
              "      R\"***("_s, link->data.name, ")***\"_s,\n"
              "      R\"***("_s, link->data.path, ")***\"_s\n"
              "    }"_s
            );
          }
        }
      }

      print(stdout,
        "\n"
        "  };\n"
        "}\n"
        "\n"_s
      );
    }

    if (have_shaders) shaders.printShadersCpp(al, stdout);

    success = true;
  }
  BREAKABLE_END

  if (success)
  {
    return 0;
  }
  else
  {
    println(stdout,
      "#error Asset Preprocessor error: see build log for detail"_s
    );
    return 1;
  }
}

// path.hpp and file.hpp are included at start of file
#include "Windows.h"
#include "filesystem.cpp"
#include "path.cpp"
